## Connectors Project
Want to transfer file from SFTP to S3? Dont do it manually, use connectors!

This is just another practice project.

### Technologies
- java 17
- spring boot
- webflux
- akka, cluster, persistence
- vavr
- testcontainers

### Running locally
- jdk 17 required
- start docker (docker-compose up -d)
- set following env variables (could be done from Intellij)
  - server.port=8099;
  - akka_port=2551;
  - akka_management_http_port=8558;
  - akka_db_host=localhost;
  - akka_db_port=5455
- add vm params (akka is not fully updated to use jdk17)
```
--add-opens=java.base/java.nio=ALL-UNNAMED
--add-opens=java.base/jdk.internal.ref=ALL-UNNAMED
--add-opens=java.base/sun.nio.ch=ALL-UNNAMED
```
- start application

Example use
- connect to sftp (use for example filezilla, host: localhost, port: 2222, protocol: sftp, user: foo, pass: pass),
- place a file example.txt (can be empty) in "upload/source" folder and create "upload/destination" folder
- request POST localhost:8099/jobs, this request will create scheduled job to transfer example file from SFTP -> SFTP
```
{
    "sourceConnectorType": "SFTP",
    "destinationConnectorType": "SFTP",
    "sourceConnectorAttributes": {
        "host": "localhost",
        "port": "2222",
        "user": "foo",
        "password": "pass",
        "path": "upload/source",
        "fileNames": "example.txt"
    },
    "targetConnectorAttributes": {
        "host": "localhost",
        "port": "2222",
        "user": "foo",
        "password": "pass",
        "path": "upload/destination",
        "prefix": "669_"
    },
    "waitForResponse": false
}
```
in response you should get Job ID, for example 5c67f61f-f0a1-4fd1-a90e-2a04089f7349;
after a while file should be transferred;
you can check this by going to upload/destination folder on sftp
- get information about scheduled job (example response below) GET localhost:8099/jobs/5c67f61f-f0a1-4fd1-a90e-2a04089f7349
```
{
    "jobId": {
        "id": "5c67f61f-f0a1-4fd1-a90e-2a04089f7349"
    },
    "jobStatus": "SUCCESS",
    "sourceType": "SFTP",
    "destinationType": "SFTP",
    "sourceAttributes": {
        "path": "upload/source",
        "fileNames": "example.txt",
        "user": "foo",
        "password": "pass",
        "host": "localhost",
        "port": "2222"
    },
    "destinationAttributes": {
        "path": "upload/destination",
        "prefix": "uploaded_",
        "user": "foo",
        "password": "pass",
        "host": "localhost",
        "port": "2222"
    },
    "transfers": {
        "eef475ad-9118-4270-8930-1dc45c5df7a2": {
            "id": "eef475ad-9118-4270-8930-1dc45c5df7a2",
            "transferStatus": "SUCCESS",
            "metadata": {
                "type": "DefaultTransferMetadata",
                "filename": "example.txt"
            },
            "retry": null
        }
    }
}
```

### todo
- [ ] http (source)
- [ ] integration tests
- [ ] localsource can only work for testing purposes, add validation
- [ ] add notification metadata, type, on which status
- [ ] upload from multipart data (source)
- [ ] simultaneous transfer processing (separate actor)
- [ ] entities visualizer (UI)
- [ ] request validation 
- [ ] configs validation (could be done together with dynami params mapping)
- [ ] dynamic params mapping or mapping to specific objects
- [ ] metrics
- [ ] add error message to TransferStatuses
- [ ] add RetryConfig
- [ ] printed stack traces should be connected somehow to job/transfer, to be able to read logs
- [ ] okta / keycloak
- [ ] connectors definition/configuration stored in db, no need to pass parameters like user, password, host on every request
- [ ] performance/stress tests
- [ ] test schema evolution -> https://doc.akka.io/docs/akka/current/persistence-schema-evolution.html
    - if we remove an event which is stored in journal it gives error or recovery
    - what if we change field
- [ ] finished jobs, should receive passivation
- [ ] multiple files to transfer or pass pattern
- [ ] add params encyption
- [ ] create job request with data (multipart)