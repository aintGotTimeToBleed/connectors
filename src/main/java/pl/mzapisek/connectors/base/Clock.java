package pl.mzapisek.connectors.base;

import java.time.Instant;

// todo wywalic ten inrefejs
public interface Clock {

  Instant now();

  class UtcClock implements Clock {
    @Override
    public Instant now() {
      return Instant.now();
    }
  }

}
