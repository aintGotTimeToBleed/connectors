package pl.mzapisek.connectors.job;

import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.http.javadsl.model.Uri;
import akka.management.javadsl.AkkaManagement;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mzapisek.connectors.base.Clock;
import pl.mzapisek.connectors.job.application.JobService;
import pl.mzapisek.connectors.job.application.connectors.destination.DestinationFactory;
import pl.mzapisek.connectors.job.application.connectors.source.SourceFactory;
import pl.mzapisek.connectors.job.application.processor.TransferProcessor;

import java.util.concurrent.CompletionStage;

@Configuration
public class ConnectorsConfiguration {

    // todo create akka config class
    @Value("${akka.port}")
    private int akkaPort;
    @Value("${akka.management.http.port}")
    private int akkaManagementPort;
    @Value("${akka.db.host}")
    private String dbHost;
    @Value("${akka.db.port}")
    private int dbPort;

    @Bean
    public Config akkaConfig() {
        var config = ConfigFactory.load();
        return ConfigFactory
                .parseString(
                        String.format("akka.remote.netty.tcp.port = %s%n", akkaPort)
                                + String.format("akka.management.http.port=%s%n", akkaManagementPort)
                                + String.format("akka.remote.artery.canonical.port=%s%n", akkaPort)
                                + String.format("jdbc-journal.slick.db.host=\"%s\"%n", dbHost)
                                + String.format("jdbc-journal.slick.db.port=%s%n", dbPort)
                                + String.format("jdbc-journal.slick.db.url=jdbc\":\"postgresql\"://\"%s\":\"%s/postgres\"?\"reWriteBatchedInserts\"=\"true%n", dbHost, dbPort))
                .withFallback(config);
    }

    @Bean(destroyMethod = "terminate")
    public ActorSystem<Void> actorSystem(Config config) {
        return ActorSystem.create(Behaviors.receive(Void.class).build(), "connectors", config);
    }

    @Bean
    public ClusterSharding clusterSharding(ActorSystem<?> actorSystem) {
        return ClusterSharding.get(actorSystem);
    }

    @Bean
    public CompletionStage<Uri> akkaManagement(ActorSystem<?> actorSystem) {
        return AkkaManagement.get(actorSystem).start();
    }

    @Bean
    public Clock clock() {
        return new Clock.UtcClock();
    }

    @Bean
    public JobService jobService(ClusterSharding sharding, TransferProcessor transferProcessor, Clock clock, SourceFactory sourceFactory, DestinationFactory destinationFactory) {
        return new JobService(sharding, transferProcessor, clock, sourceFactory, destinationFactory);
    }

    @Bean
    public TransferProcessor transferProcessor() {
        return new TransferProcessor();
    }

    @Bean
    public SourceFactory sourceFactory() {
        return new SourceFactory();
    }

    @Bean
    public DestinationFactory destinationFactory() {
        return new DestinationFactory();
    }

}
