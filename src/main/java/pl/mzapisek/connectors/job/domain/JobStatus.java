package pl.mzapisek.connectors.job.domain;

public enum JobStatus {
  CREATED, PROCESSING, SUCCESS, ERROR, RETRY
}
