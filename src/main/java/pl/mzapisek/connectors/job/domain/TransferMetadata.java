package pl.mzapisek.connectors.job.domain;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = TransferMetadata.DefaultTransferMetadata.class, name = "DefaultTransferMetadata"),
})
public sealed interface TransferMetadata extends JsonSerializable {

    String filename();

    record DefaultTransferMetadata(String filename) implements TransferMetadata {
    }

}
