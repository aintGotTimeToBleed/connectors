package pl.mzapisek.connectors.job.domain;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Retry.OnErrorRetry.class, name = "OnErrorRetry"),
        @JsonSubTypes.Type(value = Retry.StandardRetry.class, name = "StandardRetry"),
})
public sealed interface Retry extends JsonSerializable {

    record OnErrorRetry(Duration delay,
                        Instant nextProcessingTime,
                        int attempt,
                        String errorMessage,
                        Map<Integer, String> attemptToErrorMessage) implements Retry {
    }

    record StandardRetry(Duration delay, Instant nextProcessingTime) implements Retry {
    }

}
