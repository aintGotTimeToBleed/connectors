package pl.mzapisek.connectors.job.domain;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

public sealed interface JobEvent extends JsonSerializable {

    Instant createdAt();

    record TransfersAdded(Map<UUID, Transfer> transfers, Instant createdAt) implements JobEvent {
    }

    record JobStatusUpdated(JobStatus jobStatus, Instant createdAt) implements JobEvent {
    }

    record TransferUpdated(Transfer transfer, Instant createdAt) implements JobEvent {
    }

    record JobCreated(Job job, Instant createdAt) implements JobEvent {
    }

}
