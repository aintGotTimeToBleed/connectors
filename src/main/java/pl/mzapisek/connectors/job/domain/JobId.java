package pl.mzapisek.connectors.job.domain;

import java.util.UUID;

public record JobId(UUID id) implements JsonSerializable {

  public static JobId of() {
    return of(UUID.randomUUID());
  }

  public static JobId of(UUID jobId) {
    return new JobId(jobId);
  }

}
