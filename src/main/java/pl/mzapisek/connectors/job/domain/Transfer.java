package pl.mzapisek.connectors.job.domain;

import java.util.UUID;

public record Transfer(UUID id,
                       TransferStatus transferStatus,
                       TransferMetadata metadata,
                       Retry retry) implements JsonSerializable {

    public static Transfer of() {
        return new Transfer(UUID.randomUUID(), TransferStatus.CREATED, null, null);
    }

    public Transfer setStatus(TransferStatus status) {
        return new Transfer(id, status, metadata, retry);
    }

}
