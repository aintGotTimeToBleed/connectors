package pl.mzapisek.connectors.job.domain;

sealed public interface RetryableResult {

    record ErrorResult(TransferError transferError) implements RetryableResult {
    }

    record RetryResult(Retry.StandardRetry retry) implements RetryableResult {
    }

    // todo NonRetryableErrorResult, we know that we dont want to retry

}
