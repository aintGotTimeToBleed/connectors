package pl.mzapisek.connectors.job.domain;

public enum TransferStatus {
    CREATED, SCHEDULED_FOR_PROCESSING, PROCESSING, SUCCESS, ERROR, RETRY
}
