package pl.mzapisek.connectors.job.domain;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Map;
import java.util.UUID;

// to jest potrzebne tylko do testow czy faktycznie jest potrzebne?
// czy powinno tez byc extends JsonSerializable?
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = JobCommand.CreateJob.class, name = "CreateJob"),
        @JsonSubTypes.Type(value = JobCommand.AddTransfers.class, name = "AddTransfers"),
        @JsonSubTypes.Type(value = JobCommand.UpdateTransfer.class, name = "UpdateTransfer"),
        @JsonSubTypes.Type(value = JobCommand.UpdateJobStatus.class, name = "UpdateJobStatus"),
})
public sealed interface JobCommand {

    record CreateJob(JobId jobId,
                     String sourceType,
                     String destinationType,
                     Map<String, String> sourceAttributes,
                     Map<String, String> destinationAttributes,
                     boolean waitForResponse) implements JobCommand {
    }

    record AddTransfers(Map<UUID, Transfer> transfers) implements JobCommand {
    }

    record UpdateTransfer(Transfer transfer) implements JobCommand {
    }

    record UpdateJobStatus(JobStatus jobStatus) implements JobCommand {
    }

}
