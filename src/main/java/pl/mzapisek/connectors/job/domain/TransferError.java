package pl.mzapisek.connectors.job.domain;

public record TransferError(Exception exception) implements JsonSerializable {
}
