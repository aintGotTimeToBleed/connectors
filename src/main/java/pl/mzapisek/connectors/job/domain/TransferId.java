package pl.mzapisek.connectors.job.domain;

import java.util.UUID;

// todo delete this?
public record TransferId(UUID id) implements JsonSerializable {

    public static TransferId of() {
        return of(UUID.randomUUID());
    }

    public static TransferId of(UUID jobId) {
        return new TransferId(jobId);
    }

}
