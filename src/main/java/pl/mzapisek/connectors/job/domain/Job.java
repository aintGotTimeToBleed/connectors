package pl.mzapisek.connectors.job.domain;

import io.vavr.control.Either;
import pl.mzapisek.connectors.base.Clock;
import pl.mzapisek.connectors.job.domain.JobEvent.TransferUpdated;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static io.vavr.control.Either.left;
import static io.vavr.control.Either.right;

public record Job(JobId jobId,
                  JobStatus jobStatus,
                  String sourceType,
                  String destinationType,
                  Map<String, String> sourceAttributes,
                  Map<String, String> destinationAttributes,
                  Map<UUID, Transfer> transfers) implements JsonSerializable {

    public static Job of(JobId jobId) {
        return new Job(jobId, JobStatus.CREATED, null, null, Map.of(), Map.of(), Map.of());
    }

    public static Job of(JobId jobId, String sourceType, String destinationType, Map<String, String> sourceAttributes,
                         Map<String, String> destinationAttributes) {
        return new Job(jobId, JobStatus.CREATED, sourceType, destinationType, sourceAttributes, destinationAttributes, Map.of());
    }

    public static Job from(JobCommand.CreateJob createJob) {
        return Job.of(createJob.jobId(), createJob.sourceType(), createJob.destinationType(),
                Map.copyOf(createJob.sourceAttributes()), Map.copyOf(createJob.destinationAttributes()));
    }

    public Either<JobCommandError, List<JobEvent>> process(JobCommand command, Clock clock) {
        return switch (command) {
            case JobCommand.CreateJob ignored -> left(JobCommandError.JOB_EXISTS);
            case JobCommand.AddTransfers addTransfers -> handleTransfersAddition(addTransfers, clock);
            case JobCommand.UpdateTransfer updateTransfer -> handleTransferUpdate(updateTransfer, clock);
            case JobCommand.UpdateJobStatus updateJobStatus -> handleJobStatusUpdate(updateJobStatus, clock);
        };
    }

    public Job apply(JobEvent event) {
        return switch (event) {
            case JobEvent.TransfersAdded transfersAdded -> applyTransfersAddition(Map.copyOf(transfersAdded.transfers()));
            case JobEvent.JobStatusUpdated jobStatusUpdated -> applyJobStatusUpdate(jobStatusUpdated.jobStatus());
            case TransferUpdated transferUpdated -> applyTransferStatusUpdate(transferUpdated.transfer());
            case JobEvent.JobCreated ignored -> applyCreate();
        };
    }

    private Either<JobCommandError, List<JobEvent>> handleTransfersAddition(JobCommand.AddTransfers cmd, Clock clock) {
        return right(List.of(new JobEvent.TransfersAdded(cmd.transfers(), clock.now())));
    }

    private Either<JobCommandError, List<JobEvent>> handleJobStatusUpdate(JobCommand.UpdateJobStatus cmd, Clock clock) {
        return right(List.of(new JobEvent.JobStatusUpdated(cmd.jobStatus(), clock.now())));
    }

    private Either<JobCommandError, List<JobEvent>> handleTransferUpdate(JobCommand.UpdateTransfer cmd, Clock clock) {
        var transfer = transfers.get(cmd.transfer().id());
        if (transfer == null) return left(JobCommandError.TRANSFER_NOT_EXIST);

        var transferStatusUpdated = new TransferUpdated(cmd.transfer(), clock.now());
        var transferIdToTransferCopy = new HashMap<>(Map.copyOf(this.transfers));
        transferIdToTransferCopy.put(transfer.id(), cmd.transfer());

        if (!jobStatus.equals(JobStatus.ERROR)) {
            var allTransfersFinished = areAllTransfersFinishedWithSuccess(transferIdToTransferCopy);
            if (allTransfersFinished) {
                var jobStatusUpdated = new JobEvent.JobStatusUpdated(JobStatus.SUCCESS, Instant.now());
                return right(List.of(transferStatusUpdated, jobStatusUpdated));
            }
        }

        if (cmd.transfer().transferStatus().equals(TransferStatus.ERROR)) {
            var jobStatusUpdated = new JobEvent.JobStatusUpdated(JobStatus.ERROR, Instant.now());
            return right(List.of(transferStatusUpdated, jobStatusUpdated));
        }

        if (cmd.transfer().transferStatus().equals(TransferStatus.RETRY)) {
            var jobStatusUpdated = new JobEvent.JobStatusUpdated(JobStatus.RETRY, Instant.now());
            return right(List.of(transferStatusUpdated, jobStatusUpdated));
        }

        return right(List.of(transferStatusUpdated));
    }

    private Job applyTransfersAddition(Map<UUID, Transfer> transfers) {
        var transferIdToTransfer = new HashMap<>(this.transfers);
        transferIdToTransfer.putAll(transfers);
        return new Job(jobId, jobStatus, sourceType, destinationType, sourceAttributes, destinationAttributes, transferIdToTransfer);
    }

    private Job applyJobStatusUpdate(JobStatus jobStatus) {
        return new Job(jobId, jobStatus, sourceType, destinationType, sourceAttributes, destinationAttributes, transfers);
    }

    private Job applyTransferStatusUpdate(Transfer transfer) {
        var transferIdToTransfer = new HashMap<>(Map.copyOf(this.transfers));
        transferIdToTransfer.put(transfer.id(), transfer);
        return new Job(jobId, jobStatus, sourceType, destinationType, sourceAttributes, destinationAttributes, transferIdToTransfer);
    }

    private boolean areAllTransfersFinishedWithSuccess(Map<UUID, Transfer> transferIdToTransfer) {
        return transferIdToTransfer.keySet()
                .stream()
                .filter(transferId -> !transferIdToTransfer.get(transferId).transferStatus().equals(TransferStatus.SUCCESS))
                .map(transferId -> false)
                .findAny()
                .orElseGet(() -> true);
    }

    private Job applyCreate() {
        return this;
    }

}
