package pl.mzapisek.connectors.job.domain;

// todo add value to each enum
public enum JobCommandError {
    JOB_EXISTS, TRANSFER_NOT_EXIST, TRANSFER_ADDITION_ERROR, NULL_STATE_INVALID_COMMAND_ERROR
}