package pl.mzapisek.connectors.job.application.connectors.destination.sftp;

import com.jcraft.jsch.*;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import pl.mzapisek.connectors.job.application.connectors.destination.Destination;
import pl.mzapisek.connectors.job.domain.RetryableResult;
import pl.mzapisek.connectors.job.domain.Transfer;
import pl.mzapisek.connectors.job.domain.TransferError;

import java.io.OutputStream;

@RequiredArgsConstructor
public class SftpDestination implements Destination {

    private final SftpDestinationConfig config;

    private Session session;
    private ChannelSftp channel;

    @Override
    public Either<RetryableResult, OutputStream> pushData(Transfer transfer) {
        JSch jsch = new JSch();
        try {
            session = jsch.getSession(config.user(), config.host(), config.port());
            session.setPassword(config.password());
            // todo, fix, security issu
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();

            var dst = config.path() + "/" + config.prefix() + transfer.id() + "_" + transfer.metadata().filename();

            return Either.right(channel.put(dst));
        } catch (JSchException | SftpException e) {
            e.printStackTrace();
            return Either.left(new RetryableResult.ErrorResult(new TransferError(e)));
        }
    }

    @Override
    public void close() throws Exception {
        channel.exit();
        session.disconnect();
    }

}
