package pl.mzapisek.connectors.job.application.connectors.source.local;

import pl.mzapisek.connectors.job.application.connectors.source.SourceConfig;

import java.util.List;

public record LocalSourceConfig(String path, List<String> fileNames) implements SourceConfig {
}


