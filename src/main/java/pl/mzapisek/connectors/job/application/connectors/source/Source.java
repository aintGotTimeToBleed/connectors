package pl.mzapisek.connectors.job.application.connectors.source;

import io.vavr.control.Either;
import pl.mzapisek.connectors.job.domain.RetryableResult;
import pl.mzapisek.connectors.job.domain.Transfer;

import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

public interface Source extends AutoCloseable {

    Map<UUID, Transfer> createTransfers(Map<String, String> sourceConnectorAttributes);

    Either<RetryableResult, InputStream> pullData(Transfer transfer);

}
