package pl.mzapisek.connectors.job.application.connectors.destination;

import io.vavr.control.Either;
import pl.mzapisek.connectors.job.domain.RetryableResult;
import pl.mzapisek.connectors.job.domain.Transfer;

import java.io.OutputStream;

public interface Destination extends AutoCloseable {

    Either<RetryableResult, OutputStream> pushData(Transfer transfer);

}
