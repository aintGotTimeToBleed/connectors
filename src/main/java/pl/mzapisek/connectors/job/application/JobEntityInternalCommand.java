package pl.mzapisek.connectors.job.application;

import pl.mzapisek.connectors.job.domain.JobId;
import pl.mzapisek.connectors.job.domain.Transfer;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public sealed interface JobEntityInternalCommand {

    record CreateTransfers(JobId jobId,
                           String sourceConnectorType,
                           String destinationConnectorType,
                           Map<String, String> sourceConnectorAttributes,
                           Map<String, String> targetConnectorAttributes) implements JobEntityInternalCommand {
    }

    record ScheduleTransfersProcessing(JobId jobId,
                                       Map<UUID, Transfer> transfers,
                                       String sourceConnectorType,
                                       String destinationConnectorType,
                                       Map<String, String> sourceConnectorAttributes,
                                       Map<String, String> targetConnectorAttributes) implements JobEntityInternalCommand {

        public static ScheduleTransfersProcessing of(JobEntityInternalCommand.CreateTransfers cmd, Map<UUID, Transfer> transfers) {
            return new JobEntityInternalCommand.ScheduleTransfersProcessing(cmd.jobId(), transfers, cmd.sourceConnectorType(),
                    cmd.destinationConnectorType(), cmd.sourceConnectorAttributes(), cmd.targetConnectorAttributes());
        }

    }

    record ProcessTransfer(JobId jobId,
                           Transfer transfer,
                           String sourceConnectorType,
                           String destinationConnectorType,
                           Map<String, String> sourceConnectorAttributes,
                           Map<String, String> targetConnectorAttributes) implements JobEntityInternalCommand {

        public static ProcessTransfer of(JobEntityInternalCommand.ScheduleTransfersProcessing cmd, Transfer transfer) {
            return new JobEntityInternalCommand.ProcessTransfer(cmd.jobId(), transfer, cmd.sourceConnectorType(),
                    cmd.destinationConnectorType(), cmd.sourceConnectorAttributes(), cmd.targetConnectorAttributes());
        }

    }

    record RetryTransfer(JobId jobId,
                         Transfer transfer,
                         String sourceConnectorType,
                         String destinationConnectorType,
                         Map<String, String> sourceConnectorAttributes,
                         Map<String, String> targetConnectorAttributes) implements JobEntityInternalCommand {

        public RetryTransfer setTransfer(Transfer transfer) {
            return new RetryTransfer(jobId, transfer, sourceConnectorType, destinationConnectorType, sourceConnectorAttributes, targetConnectorAttributes);
        }
    }

    record RetryTransfers(JobId jobId,
                          List<Transfer> transfers,
                          String sourceType,
                          String destinationType,
                          Map<String, String> sourceAttributes,
                          Map<String, String> targetAttributes) implements JobEntityInternalCommand {
    }

}
