package pl.mzapisek.connectors.job.application.connectors.source;

import pl.mzapisek.connectors.job.application.connectors.source.local.LocalSource;
import pl.mzapisek.connectors.job.application.connectors.source.local.LocalSourceConfig;
import pl.mzapisek.connectors.job.application.connectors.source.sftp.SftpSource;
import pl.mzapisek.connectors.job.application.connectors.source.sftp.SftpSourceConfig;

public class SourceFactory {

    public Source create(String type, SourceConfig config) {
        if (type.equals("Local")) {
            return new LocalSource((LocalSourceConfig) config);
        }
        if (type.equals("SFTP")) {
            return new SftpSource((SftpSourceConfig) config);
        }
        // todo custom
        throw new RuntimeException();
    }

}
