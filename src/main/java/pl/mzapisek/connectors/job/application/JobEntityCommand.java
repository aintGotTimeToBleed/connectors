package pl.mzapisek.connectors.job.application;

import akka.actor.typed.ActorRef;
import io.vavr.control.Option;
import pl.mzapisek.connectors.job.domain.Job;
import pl.mzapisek.connectors.job.domain.JobCommand;
import pl.mzapisek.connectors.job.domain.JsonSerializable;

public sealed interface JobEntityCommand extends JsonSerializable {

    record GetJob(ActorRef<Option<Job>> replyTo) implements JobEntityCommand {
    }

    record JobCommandInternalEnvelope(JobEntityInternalCommand command) implements JobEntityCommand {
    }

    record JobCommandEnvelope(JobCommand command, ActorRef<JobEntityResponse> replyTo) implements JobEntityCommand {
    }

}
