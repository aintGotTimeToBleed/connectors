package pl.mzapisek.connectors.job.application.processor;

import io.vavr.control.Either;
import pl.mzapisek.connectors.job.application.connectors.destination.Destination;
import pl.mzapisek.connectors.job.application.connectors.source.Source;
import pl.mzapisek.connectors.job.domain.RetryableResult;
import pl.mzapisek.connectors.job.domain.Transfer;
import pl.mzapisek.connectors.job.domain.TransferError;
import pl.mzapisek.connectors.job.domain.TransferStatus;

import java.io.InputStream;
import java.io.OutputStream;

public class TransferProcessor {

    public Either<RetryableResult, Transfer> process(Source source, Destination destination, Transfer transfer) {
        Either<RetryableResult, InputStream> input = source.pullData(transfer);
        return input.fold(Either::left, inputStream -> {
            Either<RetryableResult, OutputStream> output = destination.pushData(transfer);
            return output.fold(Either::left, outputStream -> {
                try {
                    // extract to transferData method
                    byte[] buffer = new byte[8 * 1024];
                    int bytesRead;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    // todo there should be a mechanism to trigger all necessary close methods
                    // todo czy blad przy zamykaniu resourcow powinien powodowac retryable result?
                    inputStream.close();
                    outputStream.close();
                    source.close();
                    destination.close();
                } catch (Exception e) {
                    return Either.left(new RetryableResult.ErrorResult(new TransferError(e)));
                }

                return Either.right(new Transfer(transfer.id(), TransferStatus.SUCCESS, transfer.metadata(), transfer.retry()));
            });
        });
    }

}
