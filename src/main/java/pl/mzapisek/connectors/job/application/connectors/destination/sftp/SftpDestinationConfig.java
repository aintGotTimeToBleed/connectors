package pl.mzapisek.connectors.job.application.connectors.destination.sftp;

import pl.mzapisek.connectors.job.application.connectors.destination.DestinationConfig;

public record SftpDestinationConfig(String path,
                                    String prefix,
                                    String host,
                                    int port,
                                    String user,
                                    String password) implements DestinationConfig {
}
