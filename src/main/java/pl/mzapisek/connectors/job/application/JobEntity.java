package pl.mzapisek.connectors.job.application;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.TimerScheduler;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.RecoveryCompleted;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventHandlerBuilder;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import akka.persistence.typed.javadsl.ReplyEffect;
import akka.persistence.typed.javadsl.SignalHandler;
import io.vavr.control.Either;
import io.vavr.control.Option;
import pl.mzapisek.connectors.base.Clock;
import pl.mzapisek.connectors.job.application.JobEntityCommand.JobCommandInternalEnvelope;
import pl.mzapisek.connectors.job.application.connectors.destination.Destination;
import pl.mzapisek.connectors.job.application.connectors.destination.DestinationConfigFactory;
import pl.mzapisek.connectors.job.application.connectors.destination.DestinationFactory;
import pl.mzapisek.connectors.job.application.connectors.source.Source;
import pl.mzapisek.connectors.job.application.connectors.source.SourceConfigFactory;
import pl.mzapisek.connectors.job.application.connectors.source.SourceFactory;
import pl.mzapisek.connectors.job.application.processor.TransferProcessor;
import pl.mzapisek.connectors.job.domain.*;
import pl.mzapisek.connectors.job.domain.JobEvent.JobCreated;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JobEntity extends EventSourcedBehaviorWithEnforcedReplies<JobEntityCommand, JobEvent, Job> {

    public static final EntityTypeKey<JobEntityCommand> JOB_ENTITY_TYPE_KEY =
            EntityTypeKey.create(JobEntityCommand.class, "Job");

    private static final long DURATION = 10;
    private static final int MAX_ERROR_RETRY_ATTEMPTS = 3;

    private final ActorContext<JobEntityCommand> context;
    private final Clock clock;
    private final TransferProcessor transferProcessor;
    private final ActorRef<ClusterSharding.ShardCommand> shard;
    private final TimerScheduler<JobEntityCommand> timers;
    private final SourceFactory sourceFactory;
    private final DestinationFactory destinationFactory;

    public JobEntity(PersistenceId persistenceId,
                     ActorContext<JobEntityCommand> context,
                     TransferProcessor transferProcessor,
                     Clock clock,
                     ActorRef<ClusterSharding.ShardCommand> shard,
                     TimerScheduler<JobEntityCommand> timers,
                     SourceFactory sourceFactory,
                     DestinationFactory destinationFactory) {
        super(persistenceId);
        this.context = context;
        this.transferProcessor = transferProcessor;
        this.clock = clock;
        this.shard = shard;
        this.timers = timers;
        this.sourceFactory = sourceFactory;
        this.destinationFactory = destinationFactory;
    }

    public static Behavior<JobEntityCommand> create(JobId jobId, TransferProcessor transferProcessor,
                                                    Clock clock, ActorRef<ClusterSharding.ShardCommand> shard,
                                                    SourceFactory sourceFactory, DestinationFactory destinationFactory) {
        return Behaviors.withTimers(timers -> {
            return Behaviors.setup(context -> {
                PersistenceId persistenceId = PersistenceId.of(JOB_ENTITY_TYPE_KEY.name(), jobId.id().toString());
                context.getLog().info("JobEntity {} initialization started", jobId);
                return new JobEntity(persistenceId, context, transferProcessor, clock, shard, timers, sourceFactory, destinationFactory);
            });
        });
    }

    @Override
    public Job emptyState() {
        return null;
    }

    @Override
    public SignalHandler<Job> signalHandler() {
        return newSignalHandlerBuilder()
                .onSignal(
                        RecoveryCompleted.instance(),
                        state -> {
                            if (state != null && !state.jobStatus().equals(JobStatus.RETRY)) {
                                this.context.getLog().info("{} passivating", state.jobId());
                                shard.tell(new ClusterSharding.Passivate<>(context.getSelf()));
                            } else if (state != null) {
                                // todo
                                // transfers with passed retry should be processed immediately
                                // others should be retried using timers
                                // what about transfer in created, processed etc state? should we handle them on restart?
                                var transfers = state.transfers()
                                        .keySet()
                                        .stream()
                                        .filter(transferId -> state.transfers().get(transferId).transferStatus().equals(TransferStatus.RETRY))
                                        .map(transferId -> state.transfers().get(transferId))
                                        .toList();
                                this.context.getLog().info("transfers retried after recovery: {}", transfers);
                                this.context.getSelf().tell(new JobCommandInternalEnvelope(
                                        new JobEntityInternalCommand.RetryTransfers(state.jobId(), transfers, state.sourceType(),
                                                state.destinationType(), state.sourceAttributes(), state.destinationAttributes())
                                ));
                            }
                        })
                .build();
    }

    @Override
    public CommandHandlerWithReply<JobEntityCommand, JobEvent, Job> commandHandler() {
        var builder = newCommandHandlerWithReplyBuilder();

        builder.forNullState()
                .onCommand(JobEntityCommand.GetJob.class, this::returnEmptyState)
                .onCommand(JobEntityCommand.JobCommandEnvelope.class, this::processCommand);

        builder.forStateType(Job.class)
                .onCommand(JobEntityCommand.GetJob.class, this::returnState)
                .onCommand(JobEntityCommand.JobCommandEnvelope.class, this::processStateCommand)
                .onCommand(JobCommandInternalEnvelope.class, this::processStateTellCommand);

        return builder.build();
    }

    @Override
    public EventHandler<Job, JobEvent> eventHandler() {
        EventHandlerBuilder<Job, JobEvent> builder = newEventHandlerBuilder();

        builder.forNullState()
                .onEvent(JobCreated.class, JobCreated::job);

        builder.forStateType(Job.class)
                .onAnyEvent(Job::apply);

        return builder.build();
    }

    private ReplyEffect<JobEvent, Job> returnEmptyState(JobEntityCommand.GetJob createJobEnvelope) {
        return Effect().reply(createJobEnvelope.replyTo(), Option.none());
    }

    private ReplyEffect<JobEvent, Job> processCommand(JobEntityCommand.JobCommandEnvelope envelope) {
        var jobEvents = new ArrayList<JobEvent>();
        var command = envelope.command();
        if (command instanceof JobCommand.CreateJob createJob) {
            var job = Job.from(createJob);
            var jobCreated = new JobCreated(job, clock.now());
            jobEvents.add(jobCreated);

            this.context.getLog().info("job created: {}", jobCreated.job().jobId().id());

            // todo refactor very needed
            // todo should be split into two methods, no boolean flags should be used
            if (createJob.waitForResponse()) {
                var source = createSource(createJob);
                var transfers = source.createTransfers(createJob.sourceAttributes());
                var errorOrTransfersAdded = job.process(new JobCommand.AddTransfers(transfers), clock);
                return errorOrTransfersAdded.fold(error -> {
                    var errorOrJobStatusUpdated = job.process(new JobCommand.UpdateJobStatus(JobStatus.ERROR), clock);
                    jobEvents.add(errorOrJobStatusUpdated.get().get(0));
                    return Effect()
                            .persist(jobEvents)
                            .thenReply(envelope.replyTo(), param -> new JobEntityResponse.CommandRejected(JobCommandError.TRANSFER_ADDITION_ERROR));
                }, transfersAddedEvents -> {
                    // todo refactor
                    jobEvents.add(transfersAddedEvents.get(0));
                    Job jobApplied = job.apply(transfersAddedEvents.get(0));
                    Job newest = jobApplied;
                    for (var transferId : jobApplied.transfers().keySet()) {
                        var transfer = jobApplied.transfers().get(transferId);
                        var retryableResultTransferEither =
                                transferProcessor.process(source, createDestination(createJob), transfer);
                        JobCommand fold = retryableResultTransferEither.fold(
                                retryableResult -> switch (retryableResult) {
                                    case RetryableResult.ErrorResult errorResult -> processErrorResult(errorResult, transfer);
                                    case RetryableResult.RetryResult ignored -> processRetryResult(transfer);
                                },
                                processedTransfer -> {
                                    this.context.getLog().info("transfer processed correctly: {}", processedTransfer.id());
                                    return new JobCommand.UpdateTransfer(processedTransfer);
                                    // todo refactor, what on error
                                    // return errorOrTransferStatusUpdated.get();
                                });
                        Either<JobCommandError, List<JobEvent>> process = newest.process(fold, clock);
                        for (var ev : process.get()) {
                            jobEvents.add(ev);
                            newest = newest.apply(ev);
                        }
                    }
                    var latestJob = newest;
                    return Effect()
                            .persist(jobEvents)
                            .thenReply(envelope.replyTo(), param -> new JobEntityResponse.CommandProcessed(latestJob));
                });
            } else {
                var createTransfers = createCreateTransfers(createJob);
                context.getSelf().tell(new JobCommandInternalEnvelope(createTransfers));
                return Effect()
                        .persist(List.of(jobCreated))
                        .thenReply(envelope.replyTo(), param -> new JobEntityResponse.CommandScheduled(job));
            }
        } else {
            context.getLog().error("something went wrong, only create job command allowed for null state {}", command);
            return Effect().reply(envelope.replyTo(), new JobEntityResponse.CommandRejected(JobCommandError.NULL_STATE_INVALID_COMMAND_ERROR));
        }
    }

    private ReplyEffect<JobEvent, Job> returnState(Job job, JobEntityCommand.GetJob getJob) {
        return Effect().reply(getJob.replyTo(), Option.of(job));
    }

    private ReplyEffect<JobEvent, Job> processStateCommand(JobEntityCommand.JobCommandEnvelope cmd) {
        JobCommand command = cmd.command();
        // jdk 14
        if (command instanceof JobCommand.CreateJob ignored) {
            return Effect().reply(cmd.replyTo(), new JobEntityResponse.CommandRejected(JobCommandError.JOB_EXISTS));
        }
        // create custom exception
        throw new RuntimeException("undefined command");
    }

    private ReplyEffect<JobEvent, Job> processStateTellCommand(Job job, JobCommandInternalEnvelope envelope) {
        return switch (envelope.command()) {
            case JobEntityInternalCommand.CreateTransfers createTransfers -> processCreateTransfersTellCommand(job, createTransfers);
            case JobEntityInternalCommand.ScheduleTransfersProcessing scheduleTransfersProcessing -> processScheduleTransfersProcessingCommand(job, scheduleTransfersProcessing);
            case JobEntityInternalCommand.ProcessTransfer processTransfer -> processProcessTransferCommand(job, processTransfer);
            case JobEntityInternalCommand.RetryTransfer retryTransfer -> processRetryTransferCommand(job, retryTransfer);
            case JobEntityInternalCommand.RetryTransfers retryTransfers -> processRetryTransfersCommand(retryTransfers);
        };
    }

    private ReplyEffect<JobEvent, Job> processRetryTransfersCommand(JobEntityInternalCommand.RetryTransfers retryTransfers) {
        int size = retryTransfers.transfers()
                .stream()
                .peek(transfer -> context.getSelf().tell(new JobCommandInternalEnvelope(createRetryTransfer(retryTransfers, transfer))))
                .toList()
                .size();
        this.context.getLog().info("{}: transfers retried: {}", retryTransfers.jobId().id(), size);
        return Effect().noReply();
    }

    private ReplyEffect<JobEvent, Job> processCreateTransfersTellCommand(Job job, JobEntityInternalCommand.CreateTransfers cmd) {
        var sourceType = cmd.sourceConnectorType();
        var source = sourceFactory.create(sourceType, SourceConfigFactory.create(sourceType, cmd.sourceConnectorAttributes()));
        var transfers = source.createTransfers(cmd.sourceConnectorAttributes());
        var errorOrTransfersAdded = job.process(new JobCommand.AddTransfers(transfers), clock);
        var scheduleTransfersProcessing = JobEntityInternalCommand.ScheduleTransfersProcessing.of(cmd, transfers);
        context.getSelf().tell(new JobCommandInternalEnvelope(scheduleTransfersProcessing));

        return Effect().persist(errorOrTransfersAdded.get()).thenNoReply();
    }

    private ReplyEffect<JobEvent, Job> processScheduleTransfersProcessingCommand(Job job, JobEntityInternalCommand.ScheduleTransfersProcessing cmd) {
        var transferProcessingScheduleds = cmd.transfers()
                .keySet()
                .stream()
                .<List<JobEvent>>map(transferId -> {
                    context.getSelf().tell(new JobCommandInternalEnvelope(
                            JobEntityInternalCommand.ProcessTransfer.of(cmd, cmd.transfers().get(transferId))));

                    return job.process(new JobCommand.UpdateTransfer(cmd.transfers().get(transferId).setStatus(TransferStatus.SCHEDULED_FOR_PROCESSING)), clock)
                            .fold(error -> {
                                this.context.getLog().error(error.toString());
                                return List.of();
                            }, jobEvents -> jobEvents);
                })
                .flatMap(List::stream)
                .toList();

        return Effect().persist(transferProcessingScheduleds).thenNoReply();
    }

    private ReplyEffect<JobEvent, Job> processProcessTransferCommand(Job job, JobEntityInternalCommand.ProcessTransfer cmd) {
        var transfer = cmd.transfer();
        var destinationType = cmd.destinationConnectorType();
        var sourceType = cmd.sourceConnectorType();

        var retryableResultEitherTransfer = transferProcessor.process(
                sourceFactory.create(sourceType, SourceConfigFactory.create(sourceType, cmd.sourceConnectorAttributes())),
                destinationFactory.create(destinationType, DestinationConfigFactory.create(destinationType, cmd.targetConnectorAttributes())),
                transfer);

        var events = retryableResultEitherTransfer.<List<JobEvent>>fold(
                retryableResult -> switch (retryableResult) {
                    case RetryableResult.ErrorResult errorResult -> {
                        errorResult.transferError().exception().printStackTrace();

                        int firstAttempt = 1;
                        var onErrorRetry = createOnErrorRetry(errorResult.transferError(), firstAttempt,
                                Map.of(firstAttempt, errorResult.transferError().exception().getMessage()));
                        var retriedTransfer = new Transfer(transfer.id(), TransferStatus.RETRY, transfer.metadata(), onErrorRetry);

                        timers.startSingleTimer(new JobCommandInternalEnvelope(createRetryTransfer(cmd, retriedTransfer)), onErrorRetry.delay());

                        yield job.process(new JobCommand.UpdateTransfer(retriedTransfer), clock).fold(
                                error -> {
                                    this.context.getLog().error(error.toString());
                                    return List.of();
                                },
                                jobEvents -> jobEvents);
                    }
                    case RetryableResult.RetryResult retryResult -> {
                        var retriedTransfer = new Transfer(transfer.id(), TransferStatus.RETRY, transfer.metadata(), retryResult.retry());

                        timers.startSingleTimer(new JobCommandInternalEnvelope(createRetryTransfer(cmd, retriedTransfer)), retryResult.retry().delay());

                        yield job.process(new JobCommand.UpdateTransfer(retriedTransfer), clock).fold(
                                error -> {
                                    this.context.getLog().error(error.toString());
                                    return List.of();
                                },
                                jobEvents -> jobEvents);
                    }
                },
                processedTransfer -> {
                    this.context.getLog().info("transfer processed: {}", processedTransfer.id());
                    return job.process(new JobCommand.UpdateTransfer(processedTransfer), clock).fold(
                            error -> {
                                this.context.getLog().error(error.toString());
                                return List.of();
                            },
                            jobEvents -> jobEvents);
                }
        );

        return Effect().persist(events).thenNoReply();
    }

    private ReplyEffect<JobEvent, Job> processRetryTransferCommand(Job job, JobEntityInternalCommand.RetryTransfer retryTransfer) {
        this.context.getLog().info("retry transfer, attempt: {}", ((Retry.OnErrorRetry) retryTransfer.transfer().retry()).attempt());

        var transfer = retryTransfer.transfer();
        var retryableResultTransferEither =
                transferProcessor.process(createSource(retryTransfer), createDestination(retryTransfer), transfer);

        var updateTransferStatus = retryableResultTransferEither.fold(
                retryableResult -> switch (retryableResult) {
                    case RetryableResult.RetryResult retryResult -> onRetryTransferRetryResult(retryTransfer, transfer, retryResult);
                    case RetryableResult.ErrorResult errorResult -> onRetryTransferErrorResult(retryTransfer, transfer, errorResult);
                },
                processedTransfer -> {
                    this.context.getLog().info("transfer processed: {}", processedTransfer.id());
                    return new JobCommand.UpdateTransfer(processedTransfer);
                });

        return job.process(updateTransferStatus, clock).fold(error -> {
            this.context.getLog().error(error.toString());
            return Effect().persist(List.of()).thenNoReply();
        }, jobEvents -> Effect().persist(jobEvents).thenNoReply());
    }

    private JobCommand.UpdateTransfer onRetryTransferRetryResult(JobEntityInternalCommand.RetryTransfer retryTransfer, Transfer transfer, RetryableResult.RetryResult retryResult) {
        var retriedTransfer = new Transfer(transfer.id(), TransferStatus.RETRY, transfer.metadata(), retryResult.retry());
        timers.startSingleTimer(new JobCommandInternalEnvelope(
                retryTransfer.setTransfer(retriedTransfer)), retryResult.retry().delay());

        return new JobCommand.UpdateTransfer(retriedTransfer);
    }

    private JobCommand.UpdateTransfer onRetryTransferErrorResult(JobEntityInternalCommand.RetryTransfer retryTransfer,
                                                                 Transfer transfer,
                                                                 RetryableResult.ErrorResult errorResult) {
        errorResult.transferError().exception().printStackTrace();

        var previousRetry = transfer.retry();

        if (previousRetry == null) {
            this.context.getLog().error("Invalid state, retry object should not be empty: {}", transfer.id());
            return new JobCommand.UpdateTransfer(new Transfer(transfer.id(), TransferStatus.ERROR, transfer.metadata(), transfer.retry()));
        }

        return switch (previousRetry) {
            case Retry.OnErrorRetry onErrorRetry -> {
                var nextAttempt = onErrorRetry.attempt() + 1;
                var integerExceptionHashMap = new HashMap<>(onErrorRetry.attemptToErrorMessage());
                integerExceptionHashMap.put(nextAttempt, errorResult.transferError().exception().getMessage());

                if (nextAttempt <= MAX_ERROR_RETRY_ATTEMPTS) {
                    var nextOnErrorRetry = createOnErrorRetry(errorResult.transferError(), nextAttempt, integerExceptionHashMap);
                    var retriedTransfer = new Transfer(transfer.id(), TransferStatus.RETRY, transfer.metadata(), nextOnErrorRetry);

                    timers.startSingleTimer(new JobCommandInternalEnvelope(
                            retryTransfer.setTransfer(retriedTransfer)), nextOnErrorRetry.delay());

                    yield new JobCommand.UpdateTransfer(retriedTransfer);
                } else {
                    this.context.getLog().error("max number of attempts exceeded");
                    yield new JobCommand.UpdateTransfer(new Transfer(transfer.id(), TransferStatus.ERROR, transfer.metadata(), createOnErrorRetry(errorResult.transferError(), nextAttempt, integerExceptionHashMap)));
                }
            }
            case Retry.StandardRetry standardRetry -> {
                this.context.getLog().error("Invalid state, standard retry should not be processed here: {}", transfer.id());
                yield new JobCommand.UpdateTransfer(new Transfer(transfer.id(), TransferStatus.ERROR, transfer.metadata(), standardRetry));
            }
        };
    }

    private Retry.OnErrorRetry createOnErrorRetry(TransferError transferError, int attempt, Map<Integer, String> attemptToErrorMessage) {
        return new Retry.OnErrorRetry(Duration.ofSeconds(DURATION), Instant.now().plusSeconds(DURATION),
                attempt, transferError.exception().getMessage(), attemptToErrorMessage);
    }

    private JobEntityInternalCommand.RetryTransfer createRetryTransfer(JobEntityInternalCommand.ProcessTransfer cmd,
                                                                       Transfer retriedTransfer) {
        return new JobEntityInternalCommand.RetryTransfer(cmd.jobId(), retriedTransfer, cmd.sourceConnectorType(),
                cmd.destinationConnectorType(), cmd.sourceConnectorAttributes(), cmd.targetConnectorAttributes());
    }

    private JobEntityInternalCommand.RetryTransfer createRetryTransfer(JobEntityInternalCommand.RetryTransfers retryTransfers,
                                                                       Transfer transfer) {
        return new JobEntityInternalCommand.RetryTransfer(retryTransfers.jobId(), transfer, retryTransfers.sourceType(),
                retryTransfers.destinationType(), retryTransfers.sourceAttributes(), retryTransfers.targetAttributes());
    }

    private Source createSource(JobEntityInternalCommand.RetryTransfer retryTransfer) {
        return sourceFactory.create(retryTransfer.sourceConnectorType(), SourceConfigFactory.create(retryTransfer.sourceConnectorType(),
                retryTransfer.sourceConnectorAttributes()));
    }

    private Source createSource(JobCommand.CreateJob createJob) {
        return sourceFactory.create(createJob.sourceType(), SourceConfigFactory.create(createJob.sourceType(), createJob.sourceAttributes()));
    }

    private Destination createDestination(JobEntityInternalCommand.RetryTransfer retryTransfer) {
        return destinationFactory.create(retryTransfer.destinationConnectorType(), DestinationConfigFactory.create(retryTransfer.destinationConnectorType(), retryTransfer.targetConnectorAttributes()));
    }

    private Destination createDestination(JobCommand.CreateJob createJob) {
        return destinationFactory.create(createJob.destinationType(), DestinationConfigFactory.create(createJob.destinationType(), createJob.destinationAttributes()));
    }

    private JobCommand processErrorResult(RetryableResult.ErrorResult errorResult, Transfer transfer) {
        errorResult.transferError().exception().printStackTrace();
        return new JobCommand.UpdateTransfer(new Transfer(transfer.id(), TransferStatus.ERROR, transfer.metadata(), transfer.retry()));
    }

    private JobCommand processRetryResult(Transfer transfer) {
        this.context.getLog().error("retry should not occurred in ask command");
        return new JobCommand.UpdateTransfer(new Transfer(transfer.id(), TransferStatus.ERROR, transfer.metadata(), transfer.retry()));
    }

    private JobEntityInternalCommand.CreateTransfers createCreateTransfers(JobCommand.CreateJob createJob) {
        return new JobEntityInternalCommand.CreateTransfers(createJob.jobId(), createJob.sourceType(),
                createJob.destinationType(), createJob.sourceAttributes(), createJob.destinationAttributes()
        );
    }

}
