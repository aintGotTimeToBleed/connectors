package pl.mzapisek.connectors.job.application.connectors.source.sftp;

import pl.mzapisek.connectors.job.application.connectors.source.SourceConfig;

import java.util.List;

public record SftpSourceConfig(String host,
                               int port,
                               String user,
                               String password,
                               String path,
                               List<String> fileNames) implements SourceConfig {
}
