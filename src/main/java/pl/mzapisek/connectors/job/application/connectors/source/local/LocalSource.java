package pl.mzapisek.connectors.job.application.connectors.source.local;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.mzapisek.connectors.job.application.connectors.source.Source;
import pl.mzapisek.connectors.job.domain.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@AllArgsConstructor
public class LocalSource implements Source {

    private final LocalSourceConfig localSourceConfig;

    // todo extract
    @Override
    public Map<UUID, Transfer> createTransfers(Map<String, String> sourceConnectorAttributes) {
        return createTransfers(localSourceConfig).stream().collect(Collectors.toMap(Transfer::id, transfer -> transfer));
    }

    private List<Transfer> createTransfers(LocalSourceConfig config) {
        return config.fileNames()
                .stream()
                .map(fileName -> new Transfer(UUID.randomUUID(), TransferStatus.CREATED, new TransferMetadata.DefaultTransferMetadata(fileName), null))
                .toList();
    }

    @Override
    public Either<RetryableResult, InputStream> pullData(Transfer transfer) {
        try {
            var transferMetadata = (TransferMetadata.DefaultTransferMetadata) transfer.metadata();
            var sourcePath = localSourceConfig.path() + "/" + transferMetadata.filename();
            log.info("source path:" + sourcePath);
            return Either.right(new FileInputStream(sourcePath));
        } catch (Exception e) {
            return Either.left(new RetryableResult.ErrorResult(new TransferError(e)));
        }
    }

    @Override
    public void close() throws Exception {
        // noop
    }
}
