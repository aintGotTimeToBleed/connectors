package pl.mzapisek.connectors.job.application.connectors.destination.local;

import pl.mzapisek.connectors.job.application.connectors.destination.DestinationConfig;

public record LocalDestinationConfig(String path, String prefix) implements DestinationConfig {
}
