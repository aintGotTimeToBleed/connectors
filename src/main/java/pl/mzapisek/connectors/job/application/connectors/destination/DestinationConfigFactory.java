package pl.mzapisek.connectors.job.application.connectors.destination;

import pl.mzapisek.connectors.job.application.connectors.destination.local.LocalDestinationConfig;
import pl.mzapisek.connectors.job.application.connectors.destination.sftp.SftpDestinationConfig;

import java.util.Map;

public class DestinationConfigFactory {

    public static DestinationConfig create(String type, Map<String, String> params) {
        if (type.equals("Local")) {
            return new LocalDestinationConfig(params.get("path"), params.get("prefix"));
        }
        if (type.equals("SFTP")) {
            return new SftpDestinationConfig(params.get("path"), params.get("prefix"), params.get("host"), Integer.parseInt(params.get("port")),
                    params.get("user"), params.get("password"));
        }
        throw new RuntimeException();
    }

}
