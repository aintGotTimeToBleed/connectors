package pl.mzapisek.connectors.job.application;

import pl.mzapisek.connectors.job.domain.Job;
import pl.mzapisek.connectors.job.domain.JobCommandError;
import pl.mzapisek.connectors.job.domain.JsonSerializable;

public sealed interface JobEntityResponse extends JsonSerializable {

    record CommandProcessed(Job job) implements JobEntityResponse {
    }

    record CommandScheduled(Job job) implements JobEntityResponse {
    }

    record CommandRejected(JobCommandError error) implements JobEntityResponse {
    }

}
