package pl.mzapisek.connectors.job.application.connectors.destination;

import pl.mzapisek.connectors.job.application.connectors.destination.local.LocalDestination;
import pl.mzapisek.connectors.job.application.connectors.destination.local.LocalDestinationConfig;
import pl.mzapisek.connectors.job.application.connectors.destination.sftp.SftpDestination;
import pl.mzapisek.connectors.job.application.connectors.destination.sftp.SftpDestinationConfig;

public class DestinationFactory {

    public Destination create(String type, DestinationConfig config) {
        if (type.equals("Local")) {
            return new LocalDestination((LocalDestinationConfig) config);
        }
        if (type.equals("SFTP")) {
            return new SftpDestination((SftpDestinationConfig) config);
        }
        // todo custom
        throw new RuntimeException();
    }

}
