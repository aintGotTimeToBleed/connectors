package pl.mzapisek.connectors.job.application;

import akka.actor.typed.ActorRef;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.Entity;
import io.vavr.control.Option;
import pl.mzapisek.connectors.base.Clock;
import pl.mzapisek.connectors.job.api.CreateJobRequest;
import pl.mzapisek.connectors.job.application.connectors.destination.DestinationFactory;
import pl.mzapisek.connectors.job.application.connectors.source.SourceFactory;
import pl.mzapisek.connectors.job.application.processor.TransferProcessor;
import pl.mzapisek.connectors.job.domain.Job;
import pl.mzapisek.connectors.job.domain.JobCommand;
import pl.mzapisek.connectors.job.domain.JobId;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

public class JobService {

    private final ClusterSharding sharding;
    private final SourceFactory sourceFactory;
    private final DestinationFactory destinationFactory;
    private static final Duration ASK_TIMEOUT = Duration.ofSeconds(600);

    public JobService(ClusterSharding sharding, TransferProcessor transferProcessor, Clock clock,
                      SourceFactory sourceFactory, DestinationFactory destinationFactory) {
        this.sharding = sharding;
        this.sourceFactory = sourceFactory;
        this.destinationFactory = destinationFactory;
        sharding.init(Entity.of(JobEntity.JOB_ENTITY_TYPE_KEY, entityContext -> {
            var jobId = new JobId(UUID.fromString(entityContext.getEntityId()));
            return JobEntity.create(jobId, transferProcessor, clock, entityContext.getShard(), sourceFactory, destinationFactory);
        }));
    }

    public CompletionStage<JobEntityResponse> createJob(CreateJobRequest request) {
        JobCommand.CreateJob createJobCmd = createJobCommandFromRequest(request);
        return sharding
                .entityRefFor(JobEntity.JOB_ENTITY_TYPE_KEY, createJobCmd.jobId().id().toString())
                .ask((ActorRef<JobEntityResponse> param) ->
                        new JobEntityCommand.JobCommandEnvelope(createJobCmd, param), ASK_TIMEOUT);
    }

    public CompletionStage<Option<Job>> findJobBy(JobId jobId) {
        return sharding
                .entityRefFor(JobEntity.JOB_ENTITY_TYPE_KEY, jobId.id().toString())
                .ask(JobEntityCommand.GetJob::new, ASK_TIMEOUT);
    }

    private JobCommand.CreateJob createJobCommandFromRequest(CreateJobRequest request) {
        return new JobCommand.CreateJob(request.jobId() == null ? JobId.of() : JobId.of(request.jobId()),
                request.sourceConnectorType(),
                request.destinationConnectorType(),
                request.sourceConnectorAttributes(),
                request.targetConnectorAttributes(),
                request.waitForResponse());
    }

}
