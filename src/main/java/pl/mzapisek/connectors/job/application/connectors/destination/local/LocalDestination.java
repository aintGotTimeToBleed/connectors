package pl.mzapisek.connectors.job.application.connectors.destination.local;

import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import pl.mzapisek.connectors.job.application.connectors.destination.Destination;
import pl.mzapisek.connectors.job.domain.RetryableResult;
import pl.mzapisek.connectors.job.domain.Transfer;
import pl.mzapisek.connectors.job.domain.TransferError;
import pl.mzapisek.connectors.job.domain.TransferMetadata;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

@Log4j2
public class LocalDestination implements Destination {

    private final LocalDestinationConfig config;

    public LocalDestination(LocalDestinationConfig localDestinationConfig) {
        this.config = localDestinationConfig;
    }

    public Either<RetryableResult, OutputStream> pushData(Transfer transfer) {
        var transferMetadata = (TransferMetadata.DefaultTransferMetadata) transfer.metadata();
        var path = config.path() + "/" + config.prefix() + transfer.id() + "_" + transferMetadata.filename();
        log.info("target path: " + path);
        File targetFile = new File(path);
        try {
            return Either.right(new FileOutputStream(targetFile));
        } catch (Exception e) {
            return Either.left(new RetryableResult.ErrorResult(new TransferError(e)));
        }
    }

    @Override
    public void close() throws Exception {
        // noop
    }
}
