package pl.mzapisek.connectors.job.application.connectors.source.sftp;

import com.jcraft.jsch.*;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.mzapisek.connectors.job.application.connectors.source.Source;
import pl.mzapisek.connectors.job.domain.*;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
public class SftpSource implements Source, AutoCloseable {

    private final SftpSourceConfig config;

    private Session session;
    private ChannelSftp channel;

    @Override
    public Map<UUID, Transfer> createTransfers(Map<String, String> sourceConnectorAttributes) {
        return createTransfers(config).stream().collect(Collectors.toMap(Transfer::id, transfer -> transfer));
    }

    private List<Transfer> createTransfers(SftpSourceConfig config) {
        return config.fileNames()
                .stream()
                .map(fileName -> new Transfer(UUID.randomUUID(), TransferStatus.CREATED, new TransferMetadata.DefaultTransferMetadata(fileName), null))
                .toList();
    }

    @Override
    public Either<RetryableResult, InputStream> pullData(Transfer transfer) {
        JSch jsch = new JSch();
        try {
            // todo sftp init can be extracted, same thing is in source/destination
            session = jsch.getSession(config.user(), config.host(), config.port());
            session.setPassword(config.password());
            // todo, fix, security issue
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();

            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();

            // todo, path should be validated somehow, because if we send path: upload/source/ we should trim last /
            return Either.right(channel.get(config.path() + "/" + transfer.metadata().filename()));
        } catch (JSchException | SftpException e) {
            e.printStackTrace();
            return Either.left(new RetryableResult.ErrorResult(new TransferError(e)));
        }
    }

    @Override
    public void close() throws Exception {
        // todo check if sessions are closed properly
        channel.exit();
        session.disconnect();
    }

}
