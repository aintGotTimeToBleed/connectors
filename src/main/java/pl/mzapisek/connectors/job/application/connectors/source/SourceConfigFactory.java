package pl.mzapisek.connectors.job.application.connectors.source;

import pl.mzapisek.connectors.job.application.connectors.source.local.LocalSourceConfig;
import pl.mzapisek.connectors.job.application.connectors.source.sftp.SftpSourceConfig;

import java.util.List;
import java.util.Map;

public class SourceConfigFactory {

    private SourceConfigFactory() {
    }

    public static SourceConfig create(String type, Map<String, String> params) {
        return switch (type) {
            case "Local" -> new LocalSourceConfig(params.get("path"), List.of(params.get("fileNames")));
            case "SFTP" -> new SftpSourceConfig(params.get("host"), Integer.parseInt(params.get("port")),
                    params.get("user"), params.get("password"), params.get("path"), List.of(params.get("fileNames")));
            // todo custom
            default -> throw new RuntimeException("source config not found for type: " + type);
        };
    }

}
