package pl.mzapisek.connectors.job.api;

import java.util.Map;
import java.util.UUID;

public record CreateJobRequest(UUID jobId,
                               String sourceConnectorType,
                               String destinationConnectorType,
                               Map<String, String> sourceConnectorAttributes,
                               Map<String, String> targetConnectorAttributes,
                               boolean waitForResponse) {
}