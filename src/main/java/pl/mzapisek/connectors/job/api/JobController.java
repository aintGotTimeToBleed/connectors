package pl.mzapisek.connectors.job.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mzapisek.connectors.job.application.JobEntityResponse;
import pl.mzapisek.connectors.job.application.JobService;
import pl.mzapisek.connectors.job.domain.Job;
import pl.mzapisek.connectors.job.domain.JobId;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.*;

@RestController
@RequestMapping("/jobs")
public class JobController {

    private final JobService jobService;

    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    @GetMapping("{id}")
    public Mono<ResponseEntity<Job>> get(@PathVariable String id) {
        var responseEntityCompletionStage = jobService.findJobBy(JobId.of(UUID.fromString(id)))
                .thenApply(result -> result
                        .map(ok()::body)
                        .getOrElse(notFound().build()));
        return Mono.fromCompletionStage(responseEntityCompletionStage);
    }

    @PostMapping
    public Mono<ResponseEntity<?>> create(@RequestBody CreateJobRequest createJobRequest) {
        var result =
                jobService.createJob(createJobRequest)
                        .thenApply(jobEntityResponse -> switch (jobEntityResponse) {
                            case JobEntityResponse.CommandProcessed jobProcessed -> new ResponseEntity<>(jobProcessed.job(), OK);
                            case JobEntityResponse.CommandScheduled jobScheduled ->
                                    // todo job id in location header
                                    new ResponseEntity<>("Job scheduled for processing: " + jobScheduled.job().jobId().id(), CREATED);
                            case JobEntityResponse.CommandRejected rejected -> badRequest().body("Job creation failed with: " + rejected.error().name());
                        });

        return Mono.fromCompletionStage(result);
    }

    // PUT jobs/{id}/status

    //@PostMapping /jobs/upload 200
    //upload data as a multipart

}
