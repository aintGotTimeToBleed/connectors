package pl.mzapisek.connectors;

import com.jcraft.jsch.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
public class SFTPHelper {

    @RequiredArgsConstructor
    @Getter
    public static class SFTPConfig {
        private final String host;
        private final int port;
        private final String username;
        private final String password;
        private final String path;
    }

    public static boolean uploadFile(SFTPHelper.SFTPConfig config, String fileContent, String path) {
        Session session = null;
        ChannelSftp channel = null;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(config.getUsername(), config.getHost(), config.getPort());
            session.setPassword(config.getPassword());
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            channel.put(new ByteArrayInputStream(fileContent.getBytes()), path);
            return true;
        } catch (JSchException | SftpException e) {
            return false;
        } finally {
            if (channel != null) channel.exit();
            if (session != null) session.disconnect();
        }
    }

    public static String getFileAsString(SFTPHelper.SFTPConfig config, String path) {
        Session session = null;
        ChannelSftp channel = null;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(config.getUsername(), config.getHost(), config.getPort());
            session.setPassword(config.getPassword());
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            var bufferedReader = new BufferedReader(new InputStreamReader(channel.get(path)));
            return bufferedReader.lines().collect(Collectors.joining());
        } catch (JSchException | SftpException e) {
            log.error(e.getMessage());
            return "";
        } finally {
            if (channel != null) channel.exit();
            if (session != null) session.disconnect();
        }
    }

    public static List<String> listFiles(SFTPHelper.SFTPConfig config) {
        Session session = null;
        ChannelSftp channel = null;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(config.getUsername(), config.getHost(), config.getPort());
            session.setPassword(config.getPassword());
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            return new ArrayList<ChannelSftp.LsEntry>(channel.ls(config.getPath())).stream()
                    .filter(lsEntry -> !lsEntry.getAttrs().isDir())
                    .map(ChannelSftp.LsEntry::getFilename)
                    .toList();
        } catch (JSchException | SftpException e) {
            log.error(e.getMessage());
            return List.of();
        } finally {
            if (channel != null) channel.exit();
            if (session != null) session.disconnect();
        }
    }

    // todo
    public static void cleanUpSftpContainer(SFTPHelper.SFTPConfig config) {
        // list files, remove all files
    }

}
