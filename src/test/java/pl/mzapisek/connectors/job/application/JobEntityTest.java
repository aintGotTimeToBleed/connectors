package pl.mzapisek.connectors.job.application;

import akka.actor.testkit.typed.javadsl.ActorTestKit;
import akka.actor.typed.ActorRef;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.persistence.testkit.javadsl.EventSourcedBehaviorTestKit;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.vavr.control.Either;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.mzapisek.connectors.base.Clock;
import pl.mzapisek.connectors.job.application.connectors.destination.Destination;
import pl.mzapisek.connectors.job.application.connectors.destination.DestinationFactory;
import pl.mzapisek.connectors.job.application.connectors.source.Source;
import pl.mzapisek.connectors.job.application.connectors.source.SourceFactory;
import pl.mzapisek.connectors.job.application.processor.TransferProcessor;
import pl.mzapisek.connectors.job.domain.*;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JobEntityTest {

    public static final Config UNIT_TEST_AKKA_CONFIGURATION = ConfigFactory.load("unit_test_application.conf");

    private static final ActorTestKit ACTOR_TEST_KIT = ActorTestKit
            .create(EventSourcedBehaviorTestKit.config().withFallback(UNIT_TEST_AKKA_CONFIGURATION));

    @Mock
    private TransferProcessor transferProcessor;
    @Mock
    private ActorRef<ClusterSharding.ShardCommand> shard;
    @Mock
    private SourceFactory sourceFactory;
    @Mock
    private DestinationFactory destinationFactory;
    @Mock
    private Source source;
    @Mock
    private Destination destination;
    // to do wywalenia, zamiana na zwykly clock javy
    private Clock clock = new FixedClock();

    @AfterAll
    public static void cleanUp() {
        ACTOR_TEST_KIT.shutdownTestKit();
    }

    @Disabled
    @Test
    void shouldScheduleJobForProcessing_onCreateJobCommandWithoutWaiting() {
        // given
        when(sourceFactory.create(any(), any())).thenReturn(source);
        var transfer = Transfer.of();
        when(source.createTransfers(any())).thenReturn(Map.of(transfer.id(), transfer));
        var jobId = JobId.of();
        EventSourcedBehaviorTestKit<JobEntityCommand, JobEvent, Job> jobEntityKit
                = EventSourcedBehaviorTestKit.create(ACTOR_TEST_KIT.system(), JobEntity.create(jobId, transferProcessor, clock, shard, sourceFactory, destinationFactory));
        var createJobCommand = getCreateJobCommand(jobId, false);

        // when
        var result = jobEntityKit
                .<JobEntityResponse>runCommand(replyToActorRef -> new JobEntityCommand.JobCommandEnvelope(createJobCommand, replyToActorRef));

        // then
        assertThat(result.events().get(0)).isInstanceOf(JobEvent.JobCreated.class);
        assertThat(result.events().get(1)).isInstanceOf(JobEvent.TransfersAdded.class);
        assertThat(result.reply()).isInstanceOf(JobEntityResponse.CommandScheduled.class);
        assertThat(result.state().jobStatus()).isEqualTo(JobStatus.CREATED);
    }

    @Test
    void shouldProcessJobSuccessfully_onCreateJobCommandWithWaitingSet() {
        // given
        when(sourceFactory.create(any(), any())).thenReturn(source);
        when(destinationFactory.create(any(), any())).thenReturn(destination);
        var transfer = Transfer.of();
        when(source.createTransfers(any())).thenReturn(Map.of(transfer.id(), transfer));
        when(transferProcessor.process(any(), any(), any())).thenReturn(Either.right(transfer.setStatus(TransferStatus.SUCCESS)));

        var jobId = JobId.of();
        EventSourcedBehaviorTestKit<JobEntityCommand, JobEvent, Job> jobEntityKit
                = EventSourcedBehaviorTestKit.create(ACTOR_TEST_KIT.system(), JobEntity.create(jobId, transferProcessor, clock, shard, sourceFactory, destinationFactory));
        var createJobCommand = getCreateJobCommand(jobId, true);

        // when
        var result = jobEntityKit
                .<JobEntityResponse>runCommand(replyToActorRef -> new JobEntityCommand.JobCommandEnvelope(createJobCommand, replyToActorRef));

        assertThat(result.events()).hasSize(4);
        assertThat(result.events().get(0)).isInstanceOf(JobEvent.JobCreated.class);
        assertThat(result.events().get(1)).isInstanceOf(JobEvent.TransfersAdded.class);
        assertThat(result.events().get(2)).isInstanceOf(JobEvent.TransferUpdated.class);
        assertThat(result.events().get(3)).isInstanceOf(JobEvent.JobStatusUpdated.class);
        assertThat(result.reply()).isInstanceOf(JobEntityResponse.CommandProcessed.class);
        assertThat(result.state().jobStatus()).isEqualTo(JobStatus.SUCCESS);
    }

    @Test
    void shouldReturnCommandRejectedResponse_whenOtherThanCreateJobCommandIsPassedForNullState() {
        var jobId = JobId.of();
        EventSourcedBehaviorTestKit<JobEntityCommand, JobEvent, Job> jobEntityKit
                = EventSourcedBehaviorTestKit.create(ACTOR_TEST_KIT.system(), JobEntity.create(jobId, transferProcessor, clock, shard, sourceFactory, destinationFactory));

        // when
        var result = jobEntityKit
                .<JobEntityResponse>runCommand(replyToActorRef -> new JobEntityCommand.JobCommandEnvelope(new JobCommand.UpdateJobStatus(JobStatus.SUCCESS), replyToActorRef));

        // then
        assertThat(result.reply()).isInstanceOf(JobEntityResponse.CommandRejected.class);
        assertThat(((JobEntityResponse.CommandRejected) result.reply()).error()).isEqualTo(JobCommandError.NULL_STATE_INVALID_COMMAND_ERROR);
    }
    // todo test restart
    // To test recovery the restart method of the EventSourcedBehaviorTestKit can be used.
    // It will restart the behavior, which will then recover from stored snapshot and events from previous commands.
    // It’s also possible to populate the storage with events or simulate failures by using the underlying PersistenceTestKit.

    private JobCommand.CreateJob getCreateJobCommand(JobId jobId, boolean waitForResponse) {
        return new JobCommand.CreateJob(
                jobId,
                "Local",
                "Local",
                Map.of("path", "/", "fileNames", "file.txt"),
                Map.of("path", "/", "fileNames", "file.txt"),
                waitForResponse);
    }

    private JobCommand.CreateJob getInvalidCreateJobCommand(JobId jobId, boolean waitForResponse) {
        return new JobCommand.CreateJob(
                jobId,
                "Local",
                "Local",
                Map.of("path", "/", "fileNames", "file.txt"),
                Map.of("path", "/", "fileNames", "file.txt"),
                waitForResponse);
    }

}