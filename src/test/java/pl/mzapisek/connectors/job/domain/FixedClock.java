package pl.mzapisek.connectors.job.domain;

import pl.mzapisek.connectors.base.Clock;

import java.time.Instant;

public class FixedClock implements Clock {

    private final Instant now;

    public FixedClock() {
        this.now = Instant.now();
    }

    public FixedClock(Instant now) {
        this.now = now;
    }

    @Override
    public Instant now() {
        return now;
    }

}
