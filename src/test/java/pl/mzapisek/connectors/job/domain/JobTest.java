package pl.mzapisek.connectors.job.domain;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class JobTest {

    @Test
    void shouldApplyTransfers_onAddTransfersCommand() {
        // given
        var uuid = UUID.randomUUID();
        var job = Job.of(new JobId(uuid));
        var clock = new FixedClock();

        // when
        var transfer = Transfer.of();
        var jobEvents = job.process(
                new JobCommand.AddTransfers(Map.of(transfer.id(), transfer)), clock).get();
        var updatedJob = job.apply(jobEvents.get(0));

        // then
        assertThat(jobEvents).hasSize(1);
        assertThat(job.transfers()).isEmpty();
        assertThat(updatedJob.transfers()).hasSize(1);
        assertThat(job).isNotEqualTo(updatedJob);
    }

    @Test
    void shouldReturnTransferNotExistError_onUpdateNonExistentTransfer() {
        // given
        var uuid = UUID.randomUUID();
        var job = Job.of(new JobId(uuid));
        var clock = new FixedClock();

        // when
        var error =
                job.process(new JobCommand.UpdateTransfer(Transfer.of()), clock).getLeft();

        // then
        assertThat(error).isEqualTo(JobCommandError.TRANSFER_NOT_EXIST);
    }

    @Test
    void shouldReturnJobExistsError_whenTryToCreateJobOnAlreadyCreatedJob() {
        // given
        var uuid = UUID.randomUUID();
        var job = Job.of(new JobId(uuid));
        var clock = new FixedClock();

        // when
        var error = job.process(
                new JobCommand.CreateJob(JobId.of(), "none", "none", Map.of(), Map.of(), false), clock).getLeft();

        // then
        assertThat(error).isEqualTo(JobCommandError.JOB_EXISTS);
    }

    // todo more to do

}
