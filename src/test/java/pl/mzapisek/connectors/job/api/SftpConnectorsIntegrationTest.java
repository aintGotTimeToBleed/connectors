package pl.mzapisek.connectors.job.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.org.awaitility.Awaitility;
import org.testcontainers.utility.DockerImageName;
import pl.mzapisek.connectors.SFTPHelper;
import pl.mzapisek.connectors.job.domain.Job;
import pl.mzapisek.connectors.job.domain.JobStatus;
import reactor.core.publisher.Mono;

import java.net.UnknownHostException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

// refactor
// todo create junit extension
@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
class SftpConnectorsIntegrationTest {

    @LocalServerPort
    private int port;
    public WebTestClient webTestClient;

    @BeforeEach
    void beforeEach() {
        this.webTestClient = WebTestClient.bindToServer()
                .responseTimeout(Duration.ofSeconds(15))
                .baseUrl("http://localhost:" + port)
                .build();
    }

    @Container
    private static final PostgreSQLContainer<?> POSTGRE_SQL_CONTAINER =
            new PostgreSQLContainer<>(DockerImageName.parse("postgres").withTag("12"))
                    .withClasspathResourceMapping("create_journal_and_snapshot_tables.sql",
                            "/docker-entrypoint-initdb.d/akka.sql", BindMode.READ_ONLY)
                    .withDatabaseName("postgres")
                    .withUsername("admin")
                    .withPassword("admin");

    private static final String SFTP_USERNAME = "admin";
    private static final String SFTP_PASSWORD = "admin";
    private static final String SFTP_PATH = "upload";
    private static final String FILE_CONTENT = "The Batman";

    @Container
    private static final GenericContainer<?> SFTP_CONTAINER =
            new GenericContainer<>(DockerImageName.parse("atmoz/sftp").withTag("alpine-3.7"))
                    .withExposedPorts(22)
                    .withCommand(SFTP_USERNAME + ":" + SFTP_PASSWORD + ":::" + SFTP_PATH);

    @DynamicPropertySource
    static void setDynamicProperties(DynamicPropertyRegistry registry) {
        registry.add("server.port", () -> 8099);
        registry.add("akka.port", () -> 2551);
        registry.add("akka.management.http.port", () -> 8558);
        registry.add("akka.db.host", POSTGRE_SQL_CONTAINER::getHost);
        registry.add("akka.db.port", POSTGRE_SQL_CONTAINER::getFirstMappedPort);
    }

    private String requestData = """
            {
                "sourceConnectorType": "SFTP",
                "destinationConnectorType": "SFTP",
                "sourceConnectorAttributes": {
                    "host": "%s",
                    "port": "%s",
                    "user": "admin",
                    "password": "admin",
                    "path": "upload",
                    "fileNames": "source.txt"
                },
                "targetConnectorAttributes": {
                    "host": "%s",
                    "port": "%s",
                    "user": "admin",
                    "password": "admin",
                    "path": "upload",
                    "prefix": "uploaded_"
                },
                "waitForResponse": false
            }
            """;

    // todo refactor
    @Test
    void shouldTransferFileCorrectly_fromSftpToSftp() throws UnknownHostException {
        // given
        var request = String.format(requestData, SFTP_CONTAINER.getHost(), SFTP_CONTAINER.getMappedPort(22), SFTP_CONTAINER.getHost(), SFTP_CONTAINER.getMappedPort(22));
        var config = new SFTPHelper.SFTPConfig(
                SFTP_CONTAINER.getHost(),
                SFTP_CONTAINER.getMappedPort(22),
                SFTP_USERNAME,
                SFTP_PASSWORD,
                SFTP_PATH + "/");
        SFTPHelper.uploadFile(config, FILE_CONTENT, config.getPath() + "source.txt");

        // when
        var entityExchangeResult = this.webTestClient
                .post()
                .uri("/jobs")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromPublisher(Mono.just(request), String.class))
                .exchange()
                .expectStatus().isCreated()
                .expectBody().returnResult().getResponseBody();

        assertThat(entityExchangeResult).isNotNull();

        Awaitility.await()
                .pollInterval(5, TimeUnit.SECONDS)
                .atMost(15, TimeUnit.SECONDS)
                .until(() -> SFTPHelper.listFiles(config).size() == 2);

        // then
        assertThat(new String(entityExchangeResult)).contains("Job scheduled for processing:");
        var transferredFileName = SFTPHelper.listFiles(config).stream()
                .filter(filename -> filename.contains("uploaded_"))
                .findFirst();
        assertThat(transferredFileName).isPresent();
        assertThat(SFTPHelper.getFileAsString(config, config.getPath() + transferredFileName.get())).isEqualTo(FILE_CONTENT);

        var jobId = new String(entityExchangeResult).split(": ")[1].trim();
        var job = this.webTestClient
                .get()
                .uri("/jobs/" + jobId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Job.class)
                .returnResult()
                .getResponseBody();

        assertThat(job).isNotNull();
        assertThat(job.jobStatus()).isEqualTo(JobStatus.SUCCESS);

        // after
        SFTPHelper.cleanUpSftpContainer(config);
    }

    // todo some tests (retry, errors) should be extracted to dedicated suite with integration test only connector
    // but that will be possible when dynamic parameters will be added and connectors will be dynamically created
    // or stored as a bean -> List<Source> sources

}

